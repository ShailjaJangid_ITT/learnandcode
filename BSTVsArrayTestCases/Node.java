
public class Node
	{
	    Node left, right;
	    int value;
	    public Node(){
	        left = null;
	        right = null;
	        value = 0;
	    }
	 
	    public Node(int value){
	        left = null;
	        right = null;
	        this.value = value;
	    }
    
	}

