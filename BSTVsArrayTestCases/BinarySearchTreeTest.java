import org.junit.Assert;
import org.junit.Test;


public class BinarySearchTreeTest {

	@Test
	public void testInsertCase1() {
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		binarySearchTree.insert(9);
		Assert.assertEquals(binarySearchTree.getRootNode().value, 9);
	}

	@Test
	public void testInsertCase2() {
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		binarySearchTree.insert(9);
		binarySearchTree.insert(19);
		binarySearchTree.insert(15);
		Assert.assertEquals(binarySearchTree.getRootNode().right.left.value, 15);
	}
	
	@Test
	public void testInsertCase3() {
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		binarySearchTree.insert(9);
		binarySearchTree.insert(19);
		binarySearchTree.insert(15);
		binarySearchTree.insert(15);
		Assert.assertEquals(binarySearchTree.getRootNode().right.left.left.value, 15);
	}
	
	@Test
	public void testHeightOfBSTCase1() {
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		binarySearchTree.insert(9);
		binarySearchTree.insert(19);
		binarySearchTree.insert(15);
		Assert.assertEquals(binarySearchTree.heightOfBST(binarySearchTree.getRootNode()), 3);
	}
	
	@Test
	public void testHeightOfBSTCase2() {
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		binarySearchTree.insert(29);
		binarySearchTree.insert(19);
		binarySearchTree.insert(30);
		Assert.assertEquals(binarySearchTree.heightOfBST(binarySearchTree.getRootNode()), 2);
	}
	
	@Test
	public void testHeightOfBSTCase3() {
		BinarySearchTree binarySearchTree = new BinarySearchTree();
		binarySearchTree.insert(29);
		binarySearchTree.insert(19);
		binarySearchTree.insert(15);
		binarySearchTree.insert(5);
		Assert.assertEquals(binarySearchTree.heightOfBST(binarySearchTree.getRootNode()), 4);
	}
}
