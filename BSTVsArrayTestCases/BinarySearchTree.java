public class BinarySearchTree
{
    Node root;
    
    public Node getRootNode()
    {
    	return root;
    }
    public BinarySearchTree(){
        root = null;
    }
    
    public void insert(int value){
        root = insert(root, value);
    }
  
    private Node insert(Node node, int value){
        if (node == null)
            node = new Node(value);
        else
        {
            if (value <= node.value)
                node.left = insert(node.left, value);
            else
                node.right = insert(node.right, value);
        }
        return node;
    }
	public int heightOfBST(Node node) 
   {
       if (node == null)
           return 0;
       else
       {
           int heightOfLeftBST = heightOfBST(node.left);
           int heightOfRightBST = heightOfBST(node.right);
 
           if (heightOfLeftBST > heightOfRightBST)
               return (heightOfLeftBST + 1);
            else
               return (heightOfRightBST + 1);
       }
   }
    	 
}
