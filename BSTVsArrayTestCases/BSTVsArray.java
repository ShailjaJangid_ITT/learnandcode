import java.util.Scanner;


public class BSTVsArray {
	public static void main(String[] args){
		 Scanner scanner = new Scanner(System.in);
		 BinarySearchTree binarySearchTree = new BinarySearchTree();
		 int sizeOfArray = scanner.nextInt();
		 for(int i = 0 ; i < sizeOfArray ; i++){
			 binarySearchTree.insert(scanner.nextInt());
		 }
		 scanner.close();
		 System.out.println(binarySearchTree.heightOfBST(binarySearchTree.root));
	 }
}
