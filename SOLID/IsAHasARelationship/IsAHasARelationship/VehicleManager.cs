﻿using IsAHasARelationship.Vehicles;
using System;

namespace IsAHasARelationship
{
    class VehicleManager
    {
        IVehicle vehicle;
        public VehicleManager(int vehicleType)
        {
            switch (vehicleType)
            {
                case 1:
                    vehicle = new Car();
                    break;

                case 2:
                    vehicle = new Bike();
                    break;

                case 3:
                    vehicle = new Ship();
                    break;
            }
            SelectOperation();
        }

        void SelectOperation()
        {
            Console.WriteLine("Select Operation \n 1 Accelerate \n 2 Drive \n 3 Brake \n");
            int operation = Convert.ToInt32(Console.ReadLine());

            switch (operation)
            {
                case 1:
                    vehicle.accelerate();
                    break;

                case 2:
                    vehicle.drive();
                    break;

                case 3:
                    vehicle.applyBreak();
                    break;
            }
        }
    }
}
