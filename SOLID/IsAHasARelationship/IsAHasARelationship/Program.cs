﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IsAHasARelationship.Vehicles;

namespace IsAHasARelationship
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Select Vehicle: \n1 Car \n 2 Bike \n 3 Ship \n");
            int vehicleType = Convert.ToInt32(Console.ReadLine());
            
            VehicleManager manager = new VehicleManager(vehicleType);
        }
    }
}
