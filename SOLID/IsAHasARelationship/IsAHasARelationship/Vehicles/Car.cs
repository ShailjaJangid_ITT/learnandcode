﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IsAHasARelationship.Accelerate;

namespace IsAHasARelationship.Vehicles
{
    class Car : IVehicle
    {
        public void accelerate()
        {
            AccelerateCar accelerateCar = new AccelerateCar();
            accelerateCar.accelerate();
        }

        public void applyBreak()
        {
            Console.WriteLine("Applying break");
        }

        public void drive()
        {
            Console.WriteLine("driving");
        }
    }
}
