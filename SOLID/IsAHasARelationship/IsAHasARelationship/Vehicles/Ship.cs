﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IsAHasARelationship.Accelerate;

namespace IsAHasARelationship.Vehicles
{
    class Ship : IVehicle
    {
        public void accelerate()
        {
            AccelerateShip accelerateShip = new AccelerateShip();
            accelerateShip.accelerate();
        }

        public void applyBreak()
        {
            Console.WriteLine("Applying break");
        }

        public void drive()
        {
            Console.WriteLine("driving");
        }
    }
}
