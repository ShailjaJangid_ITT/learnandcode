﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyORM
{
    public class DbPoolConnection
    {
        private static IDbConnection[] Connections;
        private static int POOL_SIZE = 100;
        private static int MAX_IDLE_TIME = 10;

        private static int[] Locks;
        private static DateTime[] Dates;

        public object ConnectionString { get; private set; }

        public IDbConnection GetConnection(out int identifier)
        {
            for (int i = 0; i < POOL_SIZE; i++)
            {
                if (Interlocked.CompareExchange(ref Locks[i], 1, 0) == 0)
                {
                    if (Dates[i] != DateTime.MinValue && (DateTime.Now - Dates[i]).TotalMinutes > MAX_IDLE_TIME)
                    {
                        Connections[i].Dispose();
                        Connections[i] = null;
                    }

                    if (Connections[i] == null)
                    {
                        IDbConnection conn = CreateConnection();
                        Connections[i] = conn;
                        conn.Open();
                    }

                    Dates[i] = DateTime.Now;
                    identifier = i;
                    return Connections[i];
                }
            }

            throw new Exception("No free connections");
        }

        private IDbConnection CreateConnection()
        {
            return new MySqlConnection(this.ConnectionString);
        }

        public void FreeConnection(int identifier)
        {
            if (identifier < 0 || identifier >= POOL_SIZE)
                return;

            Interlocked.Exchange(ref Locks[identifier], 0);
        }
    }
}
