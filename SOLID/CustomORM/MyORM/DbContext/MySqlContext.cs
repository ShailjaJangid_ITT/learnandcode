﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Sockets;
using MyORM.Enum;

namespace MyORM
{
    public class MySqlContext : IDbContext
    {
        private static MySqlContext _sqlContext;
        private static SqlConnection _sqlConnection;

        private MySqlContext()
        {
            _sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
        }

        public static MySqlContext GetSqlContextInstance()
        {
            return _sqlContext ?? new MySqlContext();
        }

        public int ExecuteScalar(string commandText)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand(commandText, _sqlConnection);
                sqlCommand.ExecuteScalar();
                sqlCommand.Dispose();
                CloseConnection();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public IDataReader ExecuteReader(string commandText)
        {
            OpenConnection();
            SqlCommand sqlCommand = new SqlCommand(commandText, _sqlConnection);
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            sqlCommand.Dispose();
            return dataReader;
        }

        public int ExecuteNonQuery(string commandText)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand(commandText, _sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
                CloseConnection();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private void OpenConnection()
        {
            _sqlConnection.Open();
        }

        private void CloseConnection()
        {
            _sqlConnection.Close();
        }
    }
}
