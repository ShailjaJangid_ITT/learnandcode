﻿using System.Data;

namespace MyORM
{
    public interface IDbContext
    {
        int ExecuteScalar(string commandText);
        IDataReader ExecuteReader(string commandText);
        int ExecuteNonQuery(string commandText);
    }
}
