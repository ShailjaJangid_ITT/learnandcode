﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using MyORM.Enum;

namespace MyORM
{
    public class MyOdbcContext : IDbContext
    {
        private static MyOdbcContext _odbcContext;
        private static OdbcConnection _odbcConnection;

        private MyOdbcContext()
        {
            _odbcConnection = new OdbcConnection(ConfigurationManager.ConnectionStrings["OdbcServer"].ConnectionString);   
        }

        public static MyOdbcContext GetOdbcContextInstance()
        {
            return _odbcContext ?? new MyOdbcContext();
        }

        public string CommandBuilder(int operation, string tableName, string[] columnNames, string additionalCondition)
        {
            string commandText = string.Empty;
            switch ((DbOperations)operation)
            {
                case DbOperations.Select:
                    commandText = "SELECT ";
                    foreach (var columnName in columnNames)
                    {
                        commandText += columnName + ", ";
                    }
                    if (columnNames.Length == 0)
                        commandText += "*";
                    else
                        commandText = commandText.Remove(commandText.Length - 2, 2);
                    commandText += " FROM " + tableName;
                    if (!string.IsNullOrEmpty(additionalCondition))
                        commandText += additionalCondition;
                    return commandText;
                case DbOperations.Update:
                    commandText = "UPDATE " + tableName + " SET";
                    foreach (string columnName in columnNames)
                    {
                        commandText += columnName + "= @" + columnName + ", ";
                    }
                    commandText = commandText.Remove(commandText.Length - 2, 2);
                    if (!string.IsNullOrEmpty(additionalCondition))
                        commandText += additionalCondition;
                    return commandText;
                case DbOperations.Delete:
                    commandText = "DELETE FROM " + tableName;
                    if (!string.IsNullOrEmpty(additionalCondition))
                        commandText += additionalCondition;
                    return commandText;
                case DbOperations.Insert:
                    commandText = "INSERT INTO " + tableName + "(";
                    foreach (var columnName in columnNames)
                    {
                        commandText += columnName + ", ";
                    }
                    commandText = commandText.Remove(commandText.Length - 2, 2);
                    commandText += ") VALUES (";
                    foreach (string columnName in columnNames)
                    {
                        commandText += " @" + columnName + ", ";
                    }
                    commandText = commandText.Remove(commandText.Length - 2, 2);
                    commandText += ")";
                    return commandText;
                default:
                    return string.Empty;
            }
        }

        public int ExecuteScalar(string commandText)
        {
            try
            {
                OpenConnection();

                OdbcCommand odbcCommand = new OdbcCommand(commandText, _odbcConnection);
                odbcCommand.ExecuteScalar();

                CloseConnection();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public IDataReader ExecuteReader(string commandText)
        {
            OpenConnection();
            OdbcCommand odbcCommand = new OdbcCommand(commandText, _odbcConnection);
            OdbcDataReader dataReader = odbcCommand.ExecuteReader();
            return dataReader;
        }

        public int ExecuteNonQuery(string commandText)
        {
            try
            {
                OpenConnection();

                OdbcCommand odbcCommand = new OdbcCommand(commandText, _odbcConnection);
                odbcCommand.ExecuteNonQuery();

                CloseConnection();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private void OpenConnection()
        {
            _odbcConnection.Open();
        }

        private void CloseConnection()
        {
            _odbcConnection.Close();
        }
    }
}