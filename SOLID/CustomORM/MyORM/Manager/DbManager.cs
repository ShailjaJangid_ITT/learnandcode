﻿using System;
using System.Collections.Generic;
using System.Data;
using MyORM.Enum;

namespace MyORM.Manager
{
    public class DbManager
    {
        private readonly IDbContext _dbContext;
        
        public DbManager(int dbChoice)
        {
            DbContextFactory dbContextFactory = new DbContextFactory();
            switch (dbChoice)
            {
                case (int)DbContextType.SqlServer:
                    _dbContext = dbContextFactory.GetDbContext(DbContextType.SqlServer);
                    break;
                case (int)DbContextType.Oracle:
                    _dbContext = dbContextFactory.GetDbContext(DbContextType.Oracle);
                    break;
            }
        }

        private string GetCommandText<T>(DbOperations dbOperation, string additionalCondition)
        {
            string commandText = "";
            return commandText;
        }

        public int Add<T>(IDictionary<string, dynamic> entity)
        {
            var commandText = string.Empty;
           
            return _dbContext.ExecuteScalar(commandText.ToString());
        }

        public int Update<T>(IDictionary<string, dynamic> entity, int id)
        {
            var commandText = string.Empty;
            return _dbContext.ExecuteNonQuery(commandText.ToString());
        }

        public int Delete<T>(int id)
        {
            var commandText = string.Empty;
            return _dbContext.ExecuteNonQuery(commandText.ToString());
        }

        public IDictionary<string, dynamic> Get<T>(int id)
        {
            IDictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            return result;
        }

        public List<IDictionary<string,dynamic>> GetAll<T>()
        {
            List<IDictionary<string,dynamic>> list = new List<IDictionary<string, dynamic>>();
            return list;
        }
    }
}