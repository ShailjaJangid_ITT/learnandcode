﻿using System;
using System.Collections.Generic;
using MyORM.Models;

namespace MyORM.Manager
{
    public class PersonManager
    {
        private readonly DbManager _dbManager;

        public PersonManager(DbManager dbManager)
        {
            _dbManager = dbManager;
        }

        public int AddPerson(Person person)
        {
            return _dbManager.Add<Person>(ConvertToDictionary(person));
        }

        public int UpdatePerson(int id, Person person)
        {
            return _dbManager.Update<Person>(ConvertToDictionary(person), id);
        }

        public int DeletePerson(int id)
        {
            return _dbManager.Delete<Person>(id);
        }

        public List<Person> GetAllPersons()
        {
            var personList = new List<Person>();
            var persons = _dbManager.GetAll<Person>();
            foreach (var person in persons)
            {
                personList.Add(ConvertToObject(person));
            }
            return personList;
        }

        public Person GetPerson(int id)
        {
            return ConvertToObject(_dbManager.Get<Person>(id));
        }

        private IDictionary<string, dynamic> ConvertToDictionary(Person person)
        {
            IDictionary<string, dynamic> entityDictionary = new Dictionary<string, dynamic>();
            entityDictionary.Add("Id", person.Id);
            entityDictionary.Add("FirstName", person.FirstName);
            entityDictionary.Add("LastName", person.LastName);
            //entityDictionary.Add("Gender", person.Gender);
            return entityDictionary;
        }

        private Person ConvertToObject(IDictionary<string, dynamic> entityDictionary)
        {
            return new Person()
            {
                FirstName = entityDictionary["FirstName"],
                LastName = entityDictionary["LastName"],
                Id = Convert.ToInt32(entityDictionary["Id"]),
                //Gender = Convert.ToInt32(entityDictionary["Gender"])
            };
        }
    }
}
