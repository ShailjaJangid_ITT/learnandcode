﻿namespace MyORM.Enum
{
    public enum DbContextType
    {
        SqlServer = 1,
        Oracle = 2,
    }
}