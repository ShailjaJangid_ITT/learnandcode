﻿

namespace MyORM
{
    class Program
    {
        static void Main(string[] args)
        {
            int iConn = 0;
            try
            {
                IDbConnection conn = GetConnection(out iConn);
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "QUERY";
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                FreeConnection(iConn);
            }
        }
    }
}
