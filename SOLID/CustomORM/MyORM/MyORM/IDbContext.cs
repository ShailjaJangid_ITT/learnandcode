﻿using System.Data;

namespace MyORM
{
    public interface IDbContext
    {
        string CommandBuilder(int operation, string tableName, string[] columnNames, string additionalCondition);
        int ExecuteScalar(string commandText);
        IDataReader ExecuteReader(string commandText);
        int ExecuteNonQuery(string commandText);
    }
}
