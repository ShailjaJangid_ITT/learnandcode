﻿using System;
using MyORM.Enum;
using MyORM.Manager;
using MyORM.Models;

namespace MyORM.UI
{
    public class PersonManagement
    {
        public void Perform()
        {
            // Considering a fixed schema of the class person
            // Model Person consists of Id (int), FirstName(string) and LastName(string)
            // Allows 4 operations on the class Person - Insertion, Updation, Deletion and Projection

            //method to deal with the creation of dbcontext object
            Console.WriteLine("Enter your choice of connection\n 1. SQL Server\n 2.Odbc\n");
            int dbChoice = GetValidInputInteger();
            DbManager dbManager = new DbManager(dbChoice);
            PersonManager personManager = new PersonManager(dbManager);

            //method to handle the user interaction
            while (true)
            {
                Console.WriteLine("Enter your choice of operation\n 1.Insertion\n 2. Updation\n 3. Deletion\n 4. Display individual item 5. Display complete list\n");
                int choice = GetValidInputInteger();
                switch (choice)
                {
                    case 1:
                        {
                            Console.WriteLine("Enter details of a person");
                            Console.WriteLine("First Name :\t");
                            string firstName = GetValidInputString();
                            Console.WriteLine("Last Name :\t");
                            string lastName = GetValidInputString();
                            //Console.WriteLine("Gender :\t");
                            //Gender gender = GetGenderInput();
                            var newPerson = new Person()
                            {
                                FirstName = firstName,
                                LastName = lastName,
                                //Gender = (int)gender
                            };
                            Console.WriteLine(personManager.AddPerson(newPerson) > 0
                                ? "Added successfully"
                                : "Error in adding data");
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter Id to be updated");
                            int id = GetValidInputInteger();
                            //ask for the information to be updated
                            Console.WriteLine("Enter updated details of a person");
                            Console.WriteLine("First Name :\t");
                            string firstName = GetValidInputString();
                            Console.WriteLine("Last Name :\t");
                            string lastName = GetValidInputString();
                            Console.WriteLine("Gender :\t");
                            Gender gender = GetGenderInput();
                            //update the information
                            Console.WriteLine(personManager.UpdatePerson(id, new Person()
                            {
                                FirstName = firstName,
                                LastName = lastName,
                                //Gender = (int)gender
                            }) > 0
                                ? "Updated successfully"
                                : "Error in updating data");
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Enter id to be deleted");
                            int id = GetValidInputInteger();

                            //delete the object
                            Console.WriteLine(personManager.DeletePerson(id) > 0
                                ? "Deleted successfully"
                                : "Error in deleting data");
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("Enter id to be displayed");
                            int id = GetValidInputInteger();
                            //Get the object
                            var person = personManager.GetPerson(id);
                            //display the object
                            Console.WriteLine(string.Format("First Name: {0}\nLast Name: {1}", person.FirstName, person.LastName));
                            break;
                        }
                    case 5:
                        {
                            var persons = personManager.GetAllPersons();
                            Console.WriteLine("Complete list:");
                            Console.WriteLine("Id\t First Name\t Last Name\t Gender");
                            foreach (var person in persons)
                            {
                                Console.WriteLine(string.Format("{0} \t {1}\t {2}\t {3}\t", person.Id, person.FirstName, person.LastName));
                            }
                            break;
                        }
                    default:
                        Console.WriteLine("Please enter valid choice");
                        break;
                }
                Console.WriteLine("Press Y to exit and display result or any other key to continue adding the information");
                string input = Console.ReadLine();
                if (input != null && input.ToLower().Equals("y"))
                    break;
            }

            Console.ReadKey();
        }

        #region Private Validation Methods

        private string GetValidInputString()
        {
            string userInput;
            while (true)
            {
                userInput = Console.ReadLine();
                if (string.IsNullOrEmpty(userInput))
                {
                    Console.WriteLine("Please enter valid string");
                    continue;
                }
                break;
            }
            return userInput;
        }

        private int GetValidInputInteger()
        {
            int userIntInput = 0;
            while (true)
            {
                string userInput = Console.ReadLine();
                if (string.IsNullOrEmpty(userInput) || !Int32.TryParse(userInput, out userIntInput))
                {
                    Console.WriteLine("Please enter valid string");
                    continue;
                }
                break;
            }
            return userIntInput;
        }

        private Gender GetGenderInput()
        {
            Console.WriteLine("Press M for male and F for female");
            Gender genderInput;
            while (true)
            {
                string userInput = Console.ReadLine();
                if (string.IsNullOrEmpty(userInput) || !(userInput.ToUpper().Equals("M") || userInput.ToUpper().Equals("F")))
                {
                    Console.WriteLine("Please enter valid character");
                    continue;
                }
                genderInput = userInput.ToUpper().Equals("M") ? Gender.Male : Gender.Female;
                break;
            }
            return genderInput;
        }

        #endregion
    }
}