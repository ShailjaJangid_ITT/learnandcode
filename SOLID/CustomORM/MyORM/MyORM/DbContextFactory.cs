﻿using MyORM.Enum;

namespace MyORM
{
    public class DbContextFactory
    {
        public IDbContext GetDbContext(DbContextType dbContextType)
        {
            switch (dbContextType)
            {
                case DbContextType.SqlServer:
                    return MySqlContext.GetSqlContextInstance();
                case DbContextType.Odbc:
                    return MyOdbcContext.GetOdbcContextInstance();
                default:
                    return null;
            }
        }
    }
}
