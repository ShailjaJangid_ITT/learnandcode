﻿namespace MyORM.Enum
{
    public enum DbContextType
    {
        SqlServer = 1,
        Odbc = 2,
        Oledb = 3
    }
}