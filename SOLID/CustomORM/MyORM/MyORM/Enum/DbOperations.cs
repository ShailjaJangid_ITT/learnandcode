﻿namespace MyORM.Enum
{
    public enum DbOperations
    {
        Select = 1,
        Update = 2,
        Delete = 3,
        Insert = 4
    }
}