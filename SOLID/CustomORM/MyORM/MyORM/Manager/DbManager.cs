﻿using System;
using System.Collections.Generic;
using System.Data;
using MyORM.Enum;
using MyORM.Helpers;

namespace MyORM.Manager
{
    public class DbManager
    {
        private readonly IDbContext _dbContext;
        
        public DbManager(int dbChoice)
        {
            DbContextFactory dbContextFactory = new DbContextFactory();
            switch (dbChoice)
            {
                case (int)DbContextType.SqlServer:
                    _dbContext = dbContextFactory.GetDbContext(DbContextType.SqlServer);
                    break;
                case (int)DbContextType.Odbc:
                    _dbContext = dbContextFactory.GetDbContext(DbContextType.Odbc);
                    break;
            }
        }

        private string GetCommandText<T>(DbOperations dbOperation, string additionalCondition)
        {
            string tableName = typeof(T).Name;
            var x = typeof(T);
            var y = typeof(T).GetType();
            var columns = new List<string>();
            foreach (var column in typeof(T).GetProperties())
            {
                columns.Add(column.Name);
            }
            string commandText = _dbContext.CommandBuilder((int)dbOperation, tableName, columns.ToArray(), additionalCondition);
            return commandText;
        }

        public int Add<T>(IDictionary<string, dynamic> entity)
        {
            var commandText = new StringFormatter(GetCommandText<T>(DbOperations.Insert,String.Empty));
            foreach (var key in entity.Keys)
            {
                commandText.Add("@"+key, entity[key]);
            }
            return _dbContext.ExecuteScalar(commandText.ToString());
        }

        public int Update<T>(IDictionary<string, dynamic> entity, int id)
        {
            string additionalCondition = string.Format(SqlConstants.AdditionalCondition, id);
            var commandText = new StringFormatter(GetCommandText<T>(DbOperations.Update, additionalCondition));
            foreach (var key in entity.Keys)
            {
                commandText.Add("@" + key, entity[key]);
            }
            return _dbContext.ExecuteNonQuery(commandText.ToString());
        }

        public int Delete<T>(int id)
        {
            string additionalCondition = string.Format(SqlConstants.AdditionalCondition, id);
            var commandText = new StringFormatter(GetCommandText<T>(DbOperations.Delete, additionalCondition));
            return _dbContext.ExecuteNonQuery(commandText.ToString());
        }

        public IDictionary<string, dynamic> Get<T>(int id)
        {
            string additionalCondition = string.Format(SqlConstants.AdditionalCondition, id);
            string commandText = GetCommandText<T>(DbOperations.Select, additionalCondition);
            IDictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            using (IDataReader reader = _dbContext.ExecuteReader(commandText))
            {
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        result.Add(reader.GetName(i), reader[i].ToString());
                    }
                    break;
                }
            }
            return result;
        }

        public List<IDictionary<string,dynamic>> GetAll<T>()
        {
            string commandText = GetCommandText<T>(DbOperations.Select, String.Empty);
            List<IDictionary<string,dynamic>> list = new List<IDictionary<string, dynamic>>();
            using (IDataReader reader = _dbContext.ExecuteReader(commandText))
            {
                while (reader.Read())
                {
                    IDictionary<string, dynamic> row = new Dictionary<string, dynamic>();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                       row.Add(reader.GetName(i), reader[i].ToString());
                    }
                    list.Add(row);
                }
            }
            return list;
        }
    }
}