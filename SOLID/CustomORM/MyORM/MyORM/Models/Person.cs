﻿using MyORM.Enum;

namespace MyORM.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public int Gender { get; set; }
    }
}
