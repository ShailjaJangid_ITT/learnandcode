﻿using MyORM.UI;

namespace MyORM
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonManagement clientPersonManagement = new PersonManagement();
            clientPersonManagement.Perform();
        }
    }
}
