﻿using System;

namespace PrintersAssignment
{
    public class CopyOperation
    {
        ScanOperation scan = new ScanOperation();
        PrintOperation print = new PrintOperation();
        Paper paper = new Paper();
        public virtual bool Copy(string content)
        {
            bool isScanned = scan.Scan(content);
            if(isScanned)
            {
                bool isPrinted = print.Print(content, paper);
                if(isPrinted)
                {
                    //implemetation for copy
                    Console.WriteLine("Copying content");
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
