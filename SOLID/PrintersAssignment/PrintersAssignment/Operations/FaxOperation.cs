﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment
{
    public class FaxOperation
    {
        public virtual bool Fax(string content)
        {
            if(!string.IsNullOrEmpty(content))
            {
                Console.WriteLine("Faxing content");
                return true;
            }
            return false;
        }
    }
}
