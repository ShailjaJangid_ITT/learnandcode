﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment
{
    public class PrintOperation
    {
        public virtual bool Print(string content, Paper paper)
        {
            if (!string.IsNullOrEmpty(content))
            {
                Console.WriteLine("Printing content");
                return true;
            }
            return false;
        }
    }
}
