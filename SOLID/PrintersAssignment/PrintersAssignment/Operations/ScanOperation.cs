﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment
{
    public class ScanOperation
    {
        public virtual bool Scan(string content)
        {
            if(!string.IsNullOrEmpty(content))
            {
                Console.WriteLine("Scanning content");
                return true;
            }
            return false;
        }
    }
}
