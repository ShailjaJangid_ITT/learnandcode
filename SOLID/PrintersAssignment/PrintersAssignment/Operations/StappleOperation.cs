﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment
{
    public class StappleOperation
    {
        public virtual bool Stapple(List<Paper> papers)
        {
            if(papers != null && papers.Count > 0)
            {
                Console.WriteLine("Stappling papers");
                return true;
            }
            return false;
        }
    }
}
