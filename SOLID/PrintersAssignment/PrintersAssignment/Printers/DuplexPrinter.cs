﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment.Printers
{
    public class DuplexPrinter 
    {
        public void DuplexPrint(string content, Paper paper)
        {
            PrintOperation printOperation = new PrintOperation();
            if (paper.isExists() && paper.isBlank())
            {
                printOperation.Print(content, paper);
                Console.WriteLine("Please turn the paper");
            }
            if (paper.isExists() && paper.isBlank())
            {
                printOperation.Print(content, paper);
            }
        }

        public void Scan(string content)
        {
            ScanOperation scanOperation = new ScanOperation();
            scanOperation.Scan(content);
        }

        public void Fax(string content)
        {
            FaxOperation faxOperation = new FaxOperation();
            faxOperation.Fax(content);
        }

        public void Copy(string content)
        {
            CopyOperation copy = new CopyOperation();
            copy.Copy(content);
        }
    }
}
