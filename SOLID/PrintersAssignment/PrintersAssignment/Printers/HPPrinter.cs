﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment.Printers
{
    public class HPPrinter 
    {
        public void Scan(string content)
        {
            ScanOperation scanOperation = new ScanOperation();
            scanOperation.Scan(content);
        }

        public void Fax(string content)
        {
            FaxOperation faxOperation = new FaxOperation();
            faxOperation.Fax(content);
        }

        public void Copy(string content)
        {
            CopyOperation copy = new CopyOperation();
            copy.Copy(content);
        }

        public void SimplePrint(string content, Paper paper)
        {
            PrintOperation printOperation = new PrintOperation();
            printOperation.Print(content, paper);
        }

        public void Stapple(List<Paper> papers)
        {
            StappleOperation stappleOperation = new StappleOperation();
            stappleOperation.Stapple(papers);
        }
    }
}
