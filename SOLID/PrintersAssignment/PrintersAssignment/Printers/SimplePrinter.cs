﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment
{
    public class SimplePrinter 
    {
        
        public void Copy(string content)
        {
            CopyOperation copy = new CopyOperation();
            copy.Copy(content);
        }

        public void SimplePrint(string content, Paper paper)
        {
            PrintOperation printOperation = new PrintOperation();
            printOperation.Print(content, paper);
        }

        public void Scan(string content)
        {
            ScanOperation scanOperation = new ScanOperation();
            scanOperation.Scan(content);
        }
    }
}
