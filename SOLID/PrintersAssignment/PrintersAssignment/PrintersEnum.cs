﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintersAssignment
{
    public enum PrintersEnum
    {
        SimplePrinter=1,
        DuplexPrinter=2,
        HPPrinter=3
    }
}
