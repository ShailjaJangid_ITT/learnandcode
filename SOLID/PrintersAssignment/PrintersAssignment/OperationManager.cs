﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintersAssignment.Printers;

namespace PrintersAssignment
{
    public class OperationManager
    {
        public OperationManager(int printerType)
        {
            switch(printerType)
            {
                case 1:
                    SimplePrinter simplePrinter = new SimplePrinter();
                    SelectSimplePrinterOperation(simplePrinter);
                    break;

                case 2:
                    DuplexPrinter duplexPrinter = new DuplexPrinter();
                    SelectDuplexPrinterOperation(duplexPrinter);
                    break;

                case 3:
                    HPPrinter hpPrinter = new HPPrinter();
                    SelectHPPrinterOperation(hpPrinter);
                    break;
            }
        }

        public void SelectSimplePrinterOperation(SimplePrinter simplePrinter)
        {
            Console.WriteLine("Please select operation:");
            Console.WriteLine("1 SimplePrint \n 2 Copy \n 3 Scan \n");
            int operationType = Convert.ToInt32(Console.ReadLine());
            string content = "Hello";
            Paper paper = new Paper();

            do
            {
                switch (operationType)
                {
                    case 1:
                        simplePrinter.SimplePrint(content, paper);
                        break;

                    case 2:
                        simplePrinter.Copy(content);
                        break;

                    case 3:
                        simplePrinter.Scan(content);
                        break;

                    default:
                        Console.WriteLine("Invalid Input");
                        break;
                }
                operationType = Convert.ToInt32(Console.ReadLine());
            } while (operationType > 0);
        }

        public void SelectDuplexPrinterOperation(DuplexPrinter duplexPrinter)
        {
            Console.WriteLine("Please select operation:");
            Console.WriteLine("1 SimplePrint \n 2 Copy \n 3 Scan \n 4 Fax \n");
            int operationType = Convert.ToInt32(Console.ReadLine());
            string content = "Hello";
            Paper paper = new Paper();

            do
            {
                switch (operationType)
                {
                    case 1:
                        duplexPrinter.DuplexPrint(content, paper);
                        break;

                    case 2:
                        duplexPrinter.Copy(content);
                        break;

                    case 3:
                        duplexPrinter.Scan(content);
                        break;

                    case 4:
                        duplexPrinter.Fax(content);
                        break;
                    default:
                        Console.WriteLine("Invalid Input");
                        break;
                }
                operationType = Convert.ToInt32(Console.ReadLine());
            } while (operationType > 0);
        }

        public void SelectHPPrinterOperation(HPPrinter hpPrinter)
        {
            Console.WriteLine("Please select operation:");
            Console.WriteLine("1 SimplePrint \n 2 Copy \n 3 Scan \n 4 Fax \n 5 Stapple");
            int operationType = Convert.ToInt32(Console.ReadLine());
            string content = "Hello";
            Paper paper = new Paper();
            List<Paper> papers = new List<Paper>();

            do
            {
                switch (operationType)
                {
                    case 1:
                        hpPrinter.SimplePrint(content, paper);
                        break;

                    case 2:
                        hpPrinter.Copy(content);
                        break;

                    case 3:
                        hpPrinter.Scan(content);
                        break;

                    case 4:
                        hpPrinter.Fax(content);
                        break;

                    case 5:
                        hpPrinter.Stapple(papers);
                        break;

                    default:
                        Console.WriteLine("Invalid Input");
                        break;
                }
                operationType = Convert.ToInt32(Console.ReadLine());
            } while (operationType > 0);
        }
    }
}
