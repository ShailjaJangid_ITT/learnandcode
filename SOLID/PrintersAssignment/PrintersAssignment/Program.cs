﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintersAssignment.Printers;

namespace PrintersAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            OperationManager operationManager;
            Console.WriteLine("Available Printers:");
            Console.WriteLine(PrintersEnum.SimplePrinter + "\n" + PrintersEnum.HPPrinter + "\n" + PrintersEnum.DuplexPrinter + "\n");
            Console.WriteLine("Press 1 for SimplePrinter, 2 for HPPrinter, 3 for DuplexPrinter");

            int printerType = Convert.ToInt32(Console.ReadLine());

            do
            {
                switch (printerType)
                {
                    case 1:
                        operationManager = new OperationManager(Convert.ToInt32(PrintersEnum.SimplePrinter));
                        break;

                    case 2:
                        operationManager = new OperationManager(Convert.ToInt32(PrintersEnum.HPPrinter));
                        break;

                    case 3:
                        operationManager = new OperationManager(Convert.ToInt32(PrintersEnum.DuplexPrinter));
                        break;
                }
                printerType = Convert.ToInt32(Console.ReadLine());
            } while (printerType > 0);
            
        }
    }
}
